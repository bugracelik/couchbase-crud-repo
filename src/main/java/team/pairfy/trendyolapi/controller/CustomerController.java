package team.pairfy.trendyolapi.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import team.pairfy.trendyolapi.model.Customer;
import team.pairfy.trendyolapi.repo.CustomerRepo;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    CustomerRepo customerRepo;

    //create resources
    @PostMapping()
    public String saveCustomer(@RequestBody Customer customer) {
        customerRepo.save(customer);
        return "customer saved succsesfully";
    }

    //get resources
    @GetMapping
    public List<Customer> getCustomer() {
        return customerRepo.findAll();
    }

    @GetMapping("/{id}")
    public Customer sample(@PathVariable Integer id) {
        Optional<Customer> byId = customerRepo.findById(id);
        Customer customer = byId.get();
        return customer;
    }


    //get resources
    @GetMapping("getByCustomerName/{customername}")
    public Customer getByUserName(@PathVariable String customername) {
        return customerRepo.findCustomerByName(customername);
    }

    //delete resources
    @DeleteMapping("delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public String deleteCustomer(@PathVariable Integer id) {
        customerRepo.deleteById(id);
        return "delete successfully";
    }

    //update resources
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    public String updateUser(@RequestBody Customer customer) {
        customerRepo.save(customer);
        return "update succesfuly";
    }

}
