package team.pairfy.trendyolapi.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import team.pairfy.trendyolapi.model.User;
import team.pairfy.trendyolapi.repo.UserRepo;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserRepo userRepo;

    //create resources
    @PostMapping()
    public String saveUser(@RequestBody User user) {
        userRepo.save(user);
        return "user saved succsesfully";
    }

    //get resources
    @GetMapping
    public List<User> getUsers() {
        List<User> allDocumentOfUser = userRepo.findAll();
        return allDocumentOfUser;
    }
    //get resources
    @GetMapping("getByUserName/{username}")
    public User getByUserName(@PathVariable String username) {
        User userByName = userRepo.findUserByName(username);
        return userByName;
    }

    //delete resources
    @DeleteMapping("delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public String deleteUser(@PathVariable Integer id) {
        userRepo.deleteById(id);
        return "delete successfully";
    }

    //update resources
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    public String updateUser(@RequestBody User user) {
        userRepo.save(user);
        return "update succesfuly";
    }
}
