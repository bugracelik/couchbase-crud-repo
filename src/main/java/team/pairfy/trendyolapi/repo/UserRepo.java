package team.pairfy.trendyolapi.repo;

import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;
import team.pairfy.trendyolapi.model.User;

@Repository
public interface UserRepo extends CouchbaseRepository<User, Integer> {
    User findUserByName(String name);
}
