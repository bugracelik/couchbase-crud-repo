package team.pairfy.trendyolapi.repo;

import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;
import team.pairfy.trendyolapi.model.Customer;

import java.util.Optional;

@Repository
public interface CustomerRepo extends CouchbaseRepository<Customer, Integer> {
    Customer findCustomerByName(String name);
    Optional<Customer> findById(Integer id);
}
