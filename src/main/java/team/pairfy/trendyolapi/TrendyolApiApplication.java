package team.pairfy.trendyolapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrendyolApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(TrendyolApiApplication.class, args);
    }

}
