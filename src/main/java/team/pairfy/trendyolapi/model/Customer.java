package team.pairfy.trendyolapi.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;

@Document
@Getter
@Setter
public class Customer {

    @Id
    private Integer id;

    private String name;

    private String mobile;

}
