package team.pairfy.trendyolapi;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.json.JsonObject;
import com.couchbase.client.java.kv.GetResult;
import com.couchbase.client.java.kv.MutationResult;

public class CouchbaseConnectTest {
    public void couchbase_connect() {
        String nodeIp = "207.154.223.152";
        String userName = "Administrator";
        String passWord = "123123";

        Cluster connect = Cluster.connect(nodeIp, userName, passWord);
        Bucket bucket = connect.bucket("trendyoldb");
        Collection collection = bucket.defaultCollection();
        // Upsert Document
        MutationResult upsertResult = collection.upsert(
                "my-document",
                JsonObject.create().put("name", "mike")
        );

        // Get Document
        GetResult getResult = collection.get("my-document");
        String name = getResult.contentAsObject().getString("name");
        System.out.println(name); // name == "mike"
        System.out.println("hello");

    }

}
